# Acl management  package for Laravel Spark 6.*+

# Installation

Install this package by adding it as a custom repository to your composer.json:

    "repositories": [
        {
            "type": "vcs",
            "url": "git@gitlab.com:nitm/laravel-spark-acl.git"
        },
        ...
    ],
    ...

Next require the interaction package

    composer require nitm/laravel-spark-acl "*"

# Importing visual components
You can import the VUE components by adding the following to your `resources/assets/js/components/bootstrap.js` file

```
require('./../../../../vendor/nitm/laravel-spark-acl/resources/views/bootstrap');
```

# Publishing templates and core files

## Publishing database files to your application

    php artisan vendor:publish --provider="Nitm\SparkACL\ServiceProvider" --tag="database"

## Publishing config to your application

    php artisan vendor:publish --provider="Nitm\SparkACL\ServiceProvider" --tag="config"


## Publishing lang file to your application

    php artisan vendor:publish --provider="Nitm\SparkACL\ServiceProvider" --tag="lang"


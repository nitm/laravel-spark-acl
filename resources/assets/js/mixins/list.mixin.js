export default {
    data () {
        return {
            items: {}
        }
    },

    methods: {
        /**
         * Get the current items.
         */
        getItems() {
            this.$emit('startLoading');
            axios.get(`/acl/${this.controllerName}/all`)
                .then(response => {
                    this.items = response.data,
                    console.log(`Nitm-Spark-ACL: ${this.modelName}`, this.items);
                    this.$emit('doneLoading');
                }).catch((error) => {
                    this.$emit('loadError', error.message);
                    this.$emit('doneLoading');
                });
        },
        deleteItem(id) {
            this.$emit('startLoading');
            axios.delete(`/acl/${this.controllerName}/${id}`).then(response => {
                if(response) {
                    this.items = this.items.filter(item => item.id != id);
                }
                this.$emit('doneLoading');
            }).catch((error) => {
                this.$emit('loadError', error.message);
                this.$emit('doneLoading');
            });
        }
    },

    computed: {
        hasAny: function () {
            return this.items.data && this.items.data.length >= 1
        }
    },

    mounted() {
        //
        this.getItems();
    }
}
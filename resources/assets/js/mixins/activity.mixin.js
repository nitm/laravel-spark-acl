export default {
    data () {
        return {
            data: {},
            errors: null,
            activity: {
                isLoading: true,
                isReady: false
            }
        }
    },
    computed: {
        isAuthReady: {
            set: function () {
                this.activity.isLoading = false;
                this.activity.isReady = true;
            },
            get: function () {
                let doneLoading = this.activity.isLoading == false && this.activity.isReady == true;
                // if(this.$route.meta.auth != undefined) {
                //     return this.$auth.ready() && doneLoading;
                // } else {
                //     return doneLoading === true;
                // }
            }
        }
    },
    mounted() {
        //Listen to loading events
        this.$on('startLoading', () => {
            this.activity.isLoading = true;
            this.activity.isReady = false;
        });
        this.$on('doneLoading', () => {
            this.activity.isLoading = false;
            this.activity.isReady = true;
        });
        this.$on('loadError', () => {
            this.activity.isLoading = false;
            this.activity.isReady = true;
        });
        this.$on('searchStart', () => {
            console.info('[Search]: Start');
            this.$emit('startLoading');
        });
        this.$on('searchComplete', (result) => {
            console.info('[Search]: Complete', result);
            this.data = result;
            this.$emit('doneLoading');
        });
    }
}
import formMixin from './form.mixin';

export default {
    mixins: [formMixin],
    props: ['model', 'id'],

    /**
     * Bootstrap the component.
     */
    mounted() {
        // this.getUser();
        this.form = Object.assign(this.form, this.model);
    },

    computed: {
        hasId: function () {
            return this.form && this.form.id !== undefined;
        }
    },


    methods: {
        /**
         * Update the permission's contact information.
         */
        update(e) {
            return this.saveFormModel((data) => {
                return Spark.put(`/acl/${this.controllerName}/${data.id}`, data)
                    .then((result) => {
                        Bus.$emit(`create${this.modelName}`);
                        return result;
                    });
            }, e);
        },

        /**
         * Get the current permissions.
         */
        getModel() {
            axios.get(`/acl/${this.controllerName}/${this.form.id}`)
                .then(response => {
                    this.form = Object.assign(this.form, response.data[this.modelName]);
                    console.log(`Nitm-Spark-ACL: Edit ${this.modelName}`, this.form);
                });
        }
    }
}

import activityMixin from './activity.mixin';

export default {
    mixins: [activityMixin],
    props: {
        passedFilter: {
            type: Object,
            default: function () {
                return null
            }
        }
    },
    data () {
        const dateIcons = {
            time: 'fa fa-clock-o',
            date: 'fa fa-calendar',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-sun-o',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        };
        return {
            options: {},
            currentMenuItem: null,
            dateIcons: dateIcons,
            dates: {
                dueOn: {
                    inline: true,
                    sideBySide: true,
                    minDate: moment(),
                    useCurrent: false,
                    icons: dateIcons
                }
            },
            filterStatus: {
                submitText: 'Search',
                submitVariant: 'info'
            },
            filter: {
                s: ''
            }
        }
    },
    computed: {
        filterHasId () {
            return this.filter.id ? true : false;
        },
        filterInputsDisabled () {
            return this.activity.isLoading ? true : false;
        },
        items () {
            return this.data.items || null;
        },
        totalPages () {
            return this.data.pagination.pages
        },
        canShowPagination() {
            return this.data.pagination.pages > 1;
        },
        pagination() {
            return this.data && this.data.pagination ? this.data.pagination : {
                page: 1,
                current: 1,
                pages: 0,
                count: 0
            };
        }
    },
    methods: {
        onReset() {
          this.filter = this.filter.map(() => {return null});
        },
        toggleMenuItem(item) {
            this.currentMenuItem = item.id;
        },
        inputChange() {
            this.onSearch(new Event('submit'));
        },
        doSearch(callback, e, data) {
            try {
                e.preventDefault();
            } catch(e) {}
            this.errors = null;
            this.filterStatus.submitText = 'Searching...';
            this.$emit('searchStart');

            data = Object.assign({
                options: ['items'],
            }, utils.flattenObject(this.filter, 'filter'), data || {});

            //Explicitly add the search string parameter
            if(this.filter.s) {
                data['s'] = this.filter.s;
            }

            console.info('[Paginate Mixin]: Getting with params', data);
            return callback({
                params: data
            }).then((result) => {
                console.info('[Search]: Get Complete', result);
                this.$emit('searchComplete', result);
                this.filterStatus.submitText = 'Done';
                this.filterStatus.submitVariant = 'secondary'
                setTimeout(() => {
                    this.filterStatus.submitText = 'Search'
                    this.filterStatus.submitVariant = 'success'
                }, 2500);
                this.pagination = data.pagination || this.pagination;
            }).catch ((error) => {
                // Only hide information if response if not a 404 since the data doesn't exist anyway
                // if([404].indexOf(error.code) === -1) {
                //     this.activity.isLoading = false;
                // }
                this.errors = this.parseErrors(error.errors ? error.errors : error.message);
                this.filterStatus.submitText = 'Search';
                this.$emit('searchComplete', result);
            });
        },
        doGetMore(callback, e, data) {
            e.preventDefault();
            this.errors = null;
            this.$emit('startLoading');

            data = Object.assign({
                page: this.data.pagination.current,
            }, utils.flattenObject(this.filter), data || {});

            console.info('[Paginate Mixin]: Getting with params', data);
            return callback({
                params: data
            }).then((result) => {
                console.info('[Paginate Mixin]: Got page', data.page, result);
                this.data = result;
                this.$emit('doneLoading');
            }).catch ((error) => {
                // Only hide information if response if not a 404 since the data doesn't exist anyway
                if([404].indexOf(error.code) === -1) {
                    this.$emit('loadError');
                }
            });
        },
        getOptions(type) {
            return this.options[type];
        },
        searchComplete(result) {
            this.data = result;
            this.$emit('doneLoading');
        }
    },
    mounted () {
        this.filter = this.passedFilter || this.filter;
        this.filterStatus.submitVariant = this.filter.id ? 'success' : 'primary';
    }
}
import activityMixin from './activity.mixin'

export default {
    mixins: [activityMixin],
    data () {
        const dateIcons = {
            time: 'fa fa-clock-o',
            date: 'fa fa-calendar',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-sun-o',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        };
        return {
            options: {},
            modelType: null,
            errors: null,
            currentMenuItem: null,
            form: {},
            dateIcons: dateIcons,
            dates: {
                dueOn: {
                    inline: true,
                    sideBySide: true,
                    minDate: moment(),
                    useCurrent: false,
                    icons: dateIcons
                },
                timeOnly: {
                    inline: true,
                    sideBySide: true,
                    minDate: moment(),
                    useCurrent: false,
                    icons: dateIcons,
                    format: 'HH:mm',
                    pickDate: false
                }
            },
            formStatus: {
                submitText: 'Save',
                submitVariant: 'info'
            },
            canGetTypeOptions: true
        }
    },
    computed: {
        formHasId () {
            return this.form.id ? true : false;
        },
        formInputsDisabled () {
            return this.activity.isLoading ? true : false;
        }
    },
    methods: {
        onReset() {
          this.form = this.form.map(() => {return null});
        },
        toggleMenuItem(item) {
            this.currentMenuItem = item.id;
        },
        parseErrors(errors) {
            errors = typeof errors == 'string' ? [errors] : Object.entries(errors).map((error) => {
                if(typeof error == 'array') {
                    return error[1];
                } else if(typeof(error == 'object')) {
                    return error.hasOwnProperty(1) ? error[1].join("\n") : error.message;
                } else {
                    return typeof error == 'string' || typeof error == 'number' ? {message: error} : error;
                }
            });
            return errors;
        },
        /**
         * Validate the model
         */
         validateModel() {
             return !this.form.errors.hasErrors();
         },
        /**
         * Custom function that can be customized based on non standard routes
         */
        getAfterSaveRoute(result) {
            return {
                name: this.$route.name.replace('create', 'edit'),
                params: {
                    id: result.id
                }
            };
        },
        /**
         * Helper for saving form models.
         * Sets activity states on the component
         */
        saveFormModel(callback, e, data) {
            e.preventDefault();
            this.errors = [];
            this.formStatus.message;
            if(this.validateModel()) {
                this.$emit('startLoading');
                data = Object.assign({}, data || this.form);
                this.formStatus.submitText = 'Saving...';
                console.info('[Form Mixin]: Saving model', data);
                // Remove the errors object
                delete data.errors;
                return callback(data).then((result) => {
                    console.info('[Form Mixin]: Saved model', result);
                    this.form = new SparkForm(result.data || result);
                    this.$emit('doneLoading');
                    this.formStatus.submitText = 'Saved';
                    this.formStatus.submitVariant = 'secondary';
                    this.formStatus.message = result.message || `Succesfully saved ${this.modelName || 'item'}!`;
                    // Reset the errors manually since this is a server error not a local validation error
                    this.form.errors.errors = {};
                    setTimeout(() => {
                        this.formStatus.submitText = 'Save';
                        this.formStatus.submitVariant = 'success';
                    }, 2500);
                }).catch ((error) => {
                    console.info('[Form Mixin]: Error saving model', error);
                    // Only hide information if response if not a 404 since the data doesn't exist anyway
                    if([404].indexOf(error.code) === -1) {
                        this.$emit('loadError');
                    } else {
                        this.$emit('doneLoading');
                    }
                    this.errors = this.parseErrors(error.errors ? error.errors : error.message);
                    this.formStatus.submitText = 'Save';
                    // Reset the errors manually since this is a server error not a local validation error
                    this.form.errors.errors = {};
                });
            } else {
                console.info('[Form Mixin]: Not saving', this.errors, this.form.errors.flatten());
                return new Promise((resolve, reject) => {
                    this.errors = this.errors || this.form.errors.getErrors()
                    resolve(this.errors);
                });
            }
        },
        /**
         * Determine the form model to use for this component.
         * Model is loaded if there is an id on the route params
         */
        resolveFormModel(resolver, params, idKey) {
            this.$emit('startLoading');
            idKey = idKey || 'id';
            params = params || {
                id: this.$route.params[idKey]
            };
            if(params && ((params.params && params.params[idKey] != undefined) || (params[idKey] != undefined))) {
                return resolver(params).then((data) => {
                    this.$emit('doneLoading');
                    if(data.items instanceof Array && data.items.length) {
                        this.form = data.items[0];
                    } else if(data.items.id) {
                        this.form = data.items;
                    }
                    this.options = data.options.form || this.options;
                    Promise.resolve(this.form);
                }).catch ((error) => {
                    this.$emit('doneLoading');
                    this.errors = this.parseErrors(error);
                })
            } else {
                return new Promise((resolve, reject) => {
                    this.form = {};
                    this.$emit('doneLoading');
                    resolve(this.form)
                });
            }
        },
        getOptions(type) {

        },
        updateCheckboxList(property, item, asInt) {
            console.log("[Form Mixin]: Updating simple checkbox list item", item, item.checked, item.value);
            if(!item.checked)  {
                this.form[property].splice(this.form[property].findIndex(value => value == item.value), 1);
            } else {
                this.form[property].push(asInt ? parseInt(item.value) : item.value);
            }
        }
    },
    mounted () {
        this.formStatus.submitVariant = this.form.id ? 'success' : 'primary';
        // this.form = this.$route.params.id && this.$route.path.indexOf('create') !== -1 ? {} : this.form;
        // if(this.canGetTypeOptions) {
        //     this.getTypeOptions(this.modelType).then((options) => {
        //         this.options = options.form;
        //         this.$emit('doneLoading');
        //     });
        // } else  {
        //     this.$emit('doneLoading');
        // }
    }
}
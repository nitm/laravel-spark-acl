import formMixin from './form.mixin';

export default {
    mixins: [formMixin],
    methods: {
        /**
         * Create the model.
         */
        create(e) {
            return this.saveFormModel((data) => {
                return Spark.post(`/acl/${this.controllerName}`, data)
                    .then((result) => {
                        Bus.$emit(`create${this.modelName}`);
                        return result;
                    });
            }, e);
        },
    }
}
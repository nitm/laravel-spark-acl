
/*
 |--------------------------------------------------------------------------
 | NITM ACL Spark Components
 |--------------------------------------------------------------------------
 |
 | Here we will load the Spark components which makes up the core client
 | application. This is also a convenient spot for you to load all of
 | your components that you write while building your applications.
 */
require('./components/users/index');
require('./components/users/list');
require('./components/users/edit');
require('./components/users/create');

/** Roles */
require('./components/roles/index');
require('./components/roles/list');
require('./components/roles/edit');
require('./components/roles/create');

/** Permissions */
require('./components/permissions/index');
require('./components/permissions/list');
require('./components/permissions/edit');
require('./components/permissions/create');
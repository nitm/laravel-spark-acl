import PermissionInput from './permission-input.vue';
import activityMixin from '../../mixins/activity.mixin';
import createMixin from '../../mixins/create.mixin';

Vue.component('nitm-spark-acl-roles-create', {
    props: ['permissions'],
    mixins: [createMixin, activityMixin],
    components: {
        PermissionInput
    },
    /**
     * The component's data.
     */
    data() {
        return {
            form: new SparkForm({
                name: '',
                permissions: []
            }),
            controllerName: 'roles',
            modelName: 'Roles',
            dataAttributes: {
                singular: 'role',
                plural: 'roles'
            }
        };
    },
    methods: {
        updatePermissions(item) {
            this.updateCheckboxList('permissions', item.target, true);
        }
    }
});
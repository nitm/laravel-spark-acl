import listMixin from '../../mixins/list.mixin';
import activityMixin from '../../mixins/activity.mixin';

Vue.component('nitm-spark-acl-roles-list', {
    mixins: [listMixin, activityMixin],
    data() {
        return {
            controllerName: 'roles',
            modelName: 'Roles',
            dataAttributes: {
                singular: 'role',
                plural: 'roles'
            }
        }
    }
});
import editMixin from '../../mixins/edit.mixin';
import activityMixin from '../../mixins/activity.mixin';
import PermissionInput from './permission-input.vue';

Vue.component('nitm-spark-acl-roles-edit', {
    props: ['permissions', 'model'],
    mixins: [editMixin, activityMixin],
    components: {
        PermissionInput
    },
    /**
     * The component's data.
     */
    data() {
        return {
            form: new SparkForm({
                name: '',
                permissions: [],
            }),
            controllerName: 'roles',
            modelName: 'Roles',
            dataAttributes: {
                singular: 'role',
                plural: 'roles'
            }
        };
    },
    methods: {
        updatePermissions(item) {
            this.updateCheckboxList('permissions', item.target, true);
        }
    }
});
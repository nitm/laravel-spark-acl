import editMixin from '../../mixins/edit.mixin';
import activityMixin from '../../mixins/activity.mixin';

Vue.component('nitm-spark-acl-permissions-edit', {
    mixins: [editMixin, activityMixin],
    /**
     * The component's data.
     */
    data() {
        return {
            form: new SparkForm({
                name: ''
            }),
            controllerName: 'permissions',
            modelName: 'Permission',
            dataAttributes: {
                singular: 'permission',
                plural: 'permissions'
            }
        };
    }
});
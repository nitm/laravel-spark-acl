
import activityMixin from '../../mixins/activity.mixin';
import createMixin from '../../mixins/create.mixin';

Vue.component('nitm-spark-acl-permissions-create', {
    mixins: [createMixin, activityMixin],
    /**
     * The component's data.
     */
    data() {
        return {
            form: new SparkForm({
                name: ''
            }),
            controllerName: 'permissions',
            modelName: 'Permission',
            dataAttributes: {
                singular: 'permission',
                plural: 'permissions'
            }
        };
    }
});
import listMixin from '../../mixins/list.mixin';
import activityMixin from '../../mixins/activity.mixin';

Vue.component('nitm-spark-acl-permissions-list', {
    mixins: [listMixin, activityMixin],
    data() {
        return {
            controllerName: 'permissions',
            modelName: 'Permissions',
            dataAttributes: {
                singular: 'permission',
                plural: 'permissions'
            }
        }
    }
});
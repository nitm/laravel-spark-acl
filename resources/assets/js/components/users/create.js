import RoleInput from './role-input.vue';
import activityMixin from '../../mixins/activity.mixin';
import createMixin from '../../mixins/create.mixin';

Vue.component('nitm-spark-acl-users-create', {
    props: ['roles'],
    mixins: [createMixin, activityMixin],
    components: {
        RoleInput
    },
    /**
     * The component's data.
     */
    data() {
        return {
            form: $.extend(true, new SparkForm({
                name: '',
                email: '',
                password: '',
                password_confirmation: '',
                roles: [],
            }), Spark.forms.updateContactInformation),
            controllerName: 'users',
            modelName: 'Users',
            dataAttributes: {
                singular: 'user',
                plural: 'users'
            }
        };
    },
    methods: {
        updateRoles(item) {
            this.updateCheckboxList('roles', item.target, true);
        }
    }
});
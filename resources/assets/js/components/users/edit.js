import editMixin from '../../mixins/edit.mixin';
import activityMixin from '../../mixins/activity.mixin';
import RoleInput from './role-input.vue';

Vue.component('nitm-spark-acl-users-edit', {
    props: ['roles', 'model'],
    mixins: [editMixin, activityMixin],
    components: {
        RoleInput
    },
    /**
     * The component's data.
     */
    data() {
        return {
            form: $.extend(true, new SparkForm({
                name: '',
                email: '',
                password: '',
                password_confirmation: '',
                roles: [],
            }), Spark.forms.updateContactInformation),
            controllerName: 'users',
            modelName: 'Users',
            dataAttributes: {
                singular: 'user',
                plural: 'users'
            }
        };
    },
    methods: {

        /**
         * Get the current users.
         */
        getModel() {
            axios.get('/acl/users/'+this.id)
                .then(response => {
                    this.form = Object.assign(this.form, response.data.user);
                    this.form.password = '';
                    this.roles = response.data.roles;
                    console.log("Nitm-Spark-ACL: Edit User", this.form);
                });
        },
        updateRoles(item) {
            this.updateCheckboxList('roles', item.target, true);
        }
    }
});
import listMixin from '../../mixins/list.mixin';
import activityMixin from '../../mixins/activity.mixin';

Vue.component('nitm-spark-acl-users-list', {
    mixins: [listMixin, activityMixin],
    data() {
        return {
            controllerName: 'users',
            modelName: 'User',
            dataAttributes: {
                singular: 'user',
                plural: 'users'
            }
        }
    }
});
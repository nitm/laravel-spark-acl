
<!-- Success Message -->
<div class="alert alert-success alert-dismissible fade show" v-if="formStatus.message">
    @{{formStatus.message}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<!-- Success Message -->
<div class="alert alert-danger alert-dismissible fade show" v-if="errors && errors.length">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <ul>
        <li v-for="error in errors">
            @{{error}}
        </li>
    </ul>
</div>
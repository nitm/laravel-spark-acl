<nitm-spark-acl-permissions-list inline-template>
    <!-- permissions -->
    <div class="table-responsive" v-if="hasAny">
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
                    <th>Permission</th>
                    <th>Operation</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="item in items.data">
                    <td>@{{ item.name }}</td>
                    <td width="200">
                        <a :href="'/acl/permissions/'+item.id+'/edit'">
                            <button class="btn btn-outline-primary"
                                :disabled="activity.isLoading">
                                <i class="fa fa-cog"></i>
                            </button>
                        </a>

                        <button class="btn btn-outline-danger"
                            @click="deleteItem(item.id)"
                            :disabled="activity.isLoading">
                            <i class="fa fa-remove"></i>
                        </button>

                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div v-else>
        <p class="well">No permissions. Add one &nbsp; <a href="/acl/permissions/create" class="btn btn-outline-primary">Here</a></p>
    </div>
</nitm-spark-acl-permissions-list>
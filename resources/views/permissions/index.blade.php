@extends('spark::layouts.app')

@section('content')
    <nitm-spark-acl-permissions-index inline-template>
        <div class="spark-screen container">
            <div class="row">
                <div class="col-md-3 spark-settings-tabs">
                    @include('nitm-spark-acl::menu')
                </div>
                <!-- Tab cards -->
                <div class="col-md-9">
                    <div class="row align-items-center">
                        <div class="col">
                            <h1>Permission Management</h1>
                        </div>
                        <div class="col">
                            <a class="btn btn-outline-primary" href="/acl/permissions/create">Create</a>
                        </div>
                    </div>
                    <div class="tab-content">
                        <!-- Permissions -->
                        <div role="tabcard" class="tab-pane active" id="permissions">
                            @include('nitm-spark-acl::permissions.list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nitm-spark-acl-permissions-index>
@endsection
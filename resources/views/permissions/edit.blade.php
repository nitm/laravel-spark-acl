@extends('spark::layouts.app')

@section('content')
    <nitm-spark-acl-permissions-edit
        :model="{{ $model ?? '{}' }}"
        inline-template>
        <div class="spark-screen container">
            <div class="row">
                <div class="col-md-3 spark-settings-tabs">
                    @include('nitm-spark-acl::menu')
                </div>
                <!-- Tab cards -->
                <div class="col-md-9">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#" onClick="window.history.back()">Back</a></li>
                            <li class="breadcrumb-item active" aria-current="page">@{{form.name}}</li>
                        </ol>
                    </nav>
                    <div class="card card-default">
                        <div class="card-header">{{__('Permission')}}</div>

                        <div class="card-body">
                            @include('nitm-spark-acl::form-messages')
                            <form role="form">
                                <!-- Name -->
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">{{__('Name')}}</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" v-model="form.name" :class="{'is-invalid': form.errors.has('name')}">

                                        <span class="invalid-feedback" v-show="form.errors.has('name')">
                                            @{{ form.errors.get('name') }}
                                        </span>
                                    </div>
                                </div>

                                <!-- Update Button -->
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary"
                                                @click.prevent="update"
                                                :disabled="form.busy">

                                            {{__('Update')}}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div v-else>
                        <div class="alert alert-warning">{{__("Couldn't find user!")}}</div>
                    </div>
                </div>
            </div>
        </div>
    </nitm-spark-acl-permissions-edit>
@endsection
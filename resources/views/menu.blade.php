<!-- Tabs -->
<aside>
    <h3 class="nav-heading ">
        {{__('Manage')}}
    </h3>
    <ul class="nav flex-column mb-4 ">
        <li class="nav-item ">
            <a class="nav-link" href="/acl/users">
                <i class="fa fa-fw text-left fa-btn fa-users"></i> {{__('Users')}}
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="/acl/roles">
                <i class="fa fa-fw text-left fa-btn fa-shield"></i> {{__('Roles')}}
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="/acl/permissions">
                <i class="fa fa-fw text-left fa-btn fa-lock"></i> {{__('Permissions')}}
            </a>
        </li>
    </ul>
</aside>
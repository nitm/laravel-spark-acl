<nitm-spark-acl-roles-list inline-template>
    <!-- Roles -->
    <div class="table-responsive" v-if="hasAny">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Role</th>
                    <th>Permissions</th>
                    <th>Operation</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="item in items.data">
                    <td>@{{ item.name }}</td>
                    <td>@{{ item.permissions.map(permission => permission.name).join(', ') }}</td>
                    {{-- Retrieve array of permissions associated to a role and convert to string --}}
                    <td width="200">
                        <a :href="'/acl/roles/'+item.id+'/edit'">
                            <button class="btn btn-outline-primary"
                                :disabled="activity.isLoading">
                                <i class="fa fa-cog"></i>
                            </button>
                        </a>
                        <button class="btn btn-outline-danger"
                            @click="deleteItem(item.id)"
                            :disabled="activity.isLoading">
                            <i class="fa fa-remove"></i>
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div v-else>
        <p class="well">No roles. Add one &nbsp; <a href="/acl/roles/create" class="btn btn-outline-primary">Here</a></p>
    </div>
</nitm-spark-acl-roles-list>

<nitm-spark-acl-roles-permission-input inline-template>
    <ul class="list-group">
        <li class="list-group-item" v-for="permisssion,idx in permisssions">
            <div class="form-check">
                <input type="checkbox"
                    class="form-check-input"
                    name="permisssions[]"
                    v-model="form.permisssions[idx]"
                    :class="{'is-invalid': form.errors.has('permissions')}">
                <label class="form-check-label">
                    @{{permisssion.name}}
                </label>
            </div>
        </li>
    </ul>
    <span class="invalid-feedback" v-show="form.errors.has('permisssions')">
        @{{ form.errors.get('permisssions') }}
    </span>
</nitm-spark-acl-roles-permission-input>
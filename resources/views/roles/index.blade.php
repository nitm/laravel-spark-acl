@extends('spark::layouts.app')

@section('content')
    <nitm-spark-acl-roles-index inline-template>
        <div class="spark-screen container">
            <div class="row">
                <div class="col-md-3 spark-settings-tabs">
                    @include('nitm-spark-acl::menu')
                </div>
                <!-- Tab cards -->
                <div class="col-md-9">
                    <div class="row align-items-center">
                        <div class="col">
                            <h1>Role Management</h1>
                        </div>
                        <div class="col">
                            <a class="btn btn-outline-primary" href="/acl/roles/create">Create</a>
                        </div>
                    </div>
                    <div class="tab-content">
                        <!-- Users -->
                        <div role="tabcard" class="tab-pane active" id="roles">
                            @include('nitm-spark-acl::roles.list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nitm-spark-acl-roles-index>
@endsection
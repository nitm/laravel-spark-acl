
<nitm-spark-acl-users-role-input inline-template>
    <ul class="list-group">
        <li class="list-group-item" v-for="role,idx in roles">
            <div class="form-check">
                <input type="checkbox"
                    class="form-check-input"
                    name="roles[]"
                    v-model="form.roles[idx]"
                    :class="{'is-invalid': form.errors.has('name')}">
                <label class="form-check-label">
                    @{{role.name}}
                </label>
            </div>
        </li>
    </ul>
    <span class="invalid-feedback" v-show="form.errors.has('roles')">
        @{{ form.errors.get('roles') }}
    </span>
</nitm-spark-acl-users-role-input>
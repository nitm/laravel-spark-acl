<nitm-spark-acl-users-list inline-template>
    <!-- Users -->
    <div class="table-responsive" v-if="hasAny">
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Date/Time Added</th>
                    <th>User Roles</th>
                    <th>Operations</th>
                </tr>
            </thead>

            <tbody>
                <tr v-for="item in items.data">
                    <td>@{{ item.name }}</td>
                    <td>@{{ item.email }}</td>
                    <td>@{{ item.created_at | date}}</td>
                    <td>@{{ item.roles.map(role => role.name).join(', ') }}</td>
                    <td width="200">
                        <a :href="'/acl/users/'+item.id+'/edit'">
                            <button class="btn btn-outline-primary"
                                :disabled="activity.isLoading">
                                <i class="fa fa-cog"></i>
                            </button>
                        </a>

                        <button class="btn btn-outline-danger"
                            @click="deleteItem(item.id)"
                            :disabled="activity.isLoading">
                            <i class="fa fa-remove"></i>
                        </button>

                    </td>
                </tr>
            </tbody>

        </table>
    </div>
    <div v-else>
        <p class="well">No users. Add one &nbsp; <a href="/acl/users/create" class="btn btn-outline-primary">Here</a></p>
    </div>
</nitm-spark-acl-users-list>
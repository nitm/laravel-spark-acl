@extends('spark::layouts.app')

@section('content')
    <nitm-spark-acl-users-index inline-template>
        <div class="spark-screen container">
            <div class="row">
                <div class="col-md-3 spark-settings-tabs">
                    @include('nitm-spark-acl::menu')
                </div>
                <!-- Tab cards -->
                <div class="col-md-9">
                    <div class="row align-items-center">
                        <div class="col">
                            <h1>User Management</h1>
                        </div>
                        <div class="col">
                            <a class="btn btn-outline-primary" href="/acl/users/create">Create</a>
                        </div>
                    </div>
                    <div class="tab-content">
                        <!-- Users -->
                        <div role="tabcard" class="tab-pane active" id="users">
                            @include('nitm-spark-acl::users.list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nitm-spark-acl-users-index>
@endsection
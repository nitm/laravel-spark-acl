@extends('spark::layouts.app')

@section('content')
    <nitm-spark-acl-users-create :roles="{{ $roles ?? '{}' }}"
        inline-template>
        <div class="spark-screen container">
            <div class="row">
                <div class="col-md-3 spark-settings-tabs">
                    @include('nitm-spark-acl::menu')
                </div>
                <!-- Tab cards -->
                <div class="col-md-9">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#" onClick="window.history.back()">Back</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ __('Create') }}</li>
                        </ol>
                    </nav>
                    <div class="card card-default">
                        <div class="card-header">{{__('Contact Information')}}</div>

                        <div class="card-body">
                            @include('nitm-spark-acl::form-messages')
                            <form role="form">
                                <!-- Name -->
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">{{__('Name')}}</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" v-model="form.name" :class="{'is-invalid': form.errors.has('name')}">

                                        <span class="invalid-feedback" v-show="form.errors.has('name')">
                                            @{{ form.errors.get('name') }}
                                        </span>
                                    </div>
                                </div>
                                <!-- Email -->
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">{{__('Email')}}</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="email" v-model="form.email" :class="{'is-invalid': form.errors.has('email')}">

                                        <span class="invalid-feedback" v-show="form.errors.has('email')">
                                            @{{ form.errors.get('email') }}
                                        </span>
                                    </div>
                                </div>

                                <!-- Phone -->
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">{{__('Phone')}}</label>

                                    <div class="col-md-6">
                                        <input type="tel" class="form-control" name="phone" v-model="form.phone" :class="{'is-invalid': form.errors.has('phone')}">

                                        <span class="invalid-feedback" v-show="form.errors.has('phone')">
                                            @{{ form.errors.get('phone') }}
                                        </span>
                                    </div>
                                </div>

                                <!-- Roles -->
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <h5><b>Roles</b></h5>
                                    </label>
                                    <div class="col-md-8">
                                        <role-input :roles="roles"
                                            :form="form"
                                            @update:roles="updateRoles"/>
                                    </div>
                                </div>
                                <!-- Password -->
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">{{__('Password')}}</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="password" type="password" v-model="form.password" :class="{'is-invalid': form.errors.has('password')}">

                                        <span class="invalid-feedback" v-show="form.errors.has('password')">
                                            @{{ form.errors.get('password') }}
                                        </span>
                                    </div>
                                </div>

                                <!-- Password Confirmation -->
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">{{__('Password Confirmation')}}</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="password_confirmation" type="password" v-model="form.password_confirmation" :class="{'is-invalid': form.errors.has('password')}">

                                        <span class="invalid-feedback" v-show="form.errors.has('password_confirmation')">
                                            @{{ form.errors.get('password_confirmation') }}
                                        </span>
                                    </div>
                                </div>

                                <!-- Update Button -->
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary"
                                                @click.prevent="create"
                                                :disabled="form.busy">

                                            {{__('Create')}}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div v-else>
                        <div class="alert alert-warning">{{__("Couldn't find user!")}}</div>
                    </div>
                </div>
            </div>
        </div>
    </nitm-spark-acl-users-create>
@endsection
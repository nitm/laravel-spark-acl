<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::prefix('acl/api')->middleware('auth:api')->group(function () {
//     Route::apiResources([
//         'users' => 'Nitm\SparkACL\Http\Controllers\UserController',
//         'roles' => 'Nitm\SparkACL\Http\Controllers\RoleController',
//         'permissions' => 'Nitm\SparkACL\Http\Controllers\PermissionController@index'
//     ]);
// });

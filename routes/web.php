<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/home', 'HomeController@index');

Route::prefix('acl')->middleware(['web'])->group(function () {
    // Need to redgister custom routes before registering resoruce routes
    Route::get('users/all', 'Nitm\SparkACL\Http\Controllers\UserController@all');
    Route::get('permissions/all', 'Nitm\SparkACL\Http\Controllers\PermissionController@all');
    Route::get('roles/all', 'Nitm\SparkACL\Http\Controllers\RoleController@all');

    Route::resources([
        'users' => 'Nitm\SparkACL\Http\Controllers\UserController',
        'roles' => 'Nitm\SparkACL\Http\Controllers\RoleController',
        'permissions' => 'Nitm\SparkACL\Http\Controllers\PermissionController'
    ]);
});

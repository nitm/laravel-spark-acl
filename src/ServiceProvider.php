<?php

namespace Nitm\SparkACL;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Illuminate\Support\Facades\Schema;

class ServiceProvider extends BaseServiceProvider
{
    /**
    * Bootstrap any application services.
    *
    * @return void
    */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

        /**
        * Publish views
        */
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'nitm-spark-acl');

        /**
        * Load tranlations
        */
        $this->loadTranslationsFrom(__DIR__.'/../publishes/resources/lang', 'nitm-spark-acl');

        $this->publishes([
        __DIR__.'/../publishes/resources/lang' => resource_path('lang/vendor/nitm-spark-acl'),
        ], 'lang');

        /**
        * Publish config
        *
        */
        $this->publishes([
        __DIR__.'/../publishes/config/laravel-permission.php' => config_path('laravel-permission.php')
        ], 'config');
        /**
        * Publish database files
        *
        */
        $this->publishes([
        __DIR__.'/../publishes/database' => base_path('database')
        ], 'database');
    }

    /**
    * Register any application services.
    *
    * @return void
    */
    public function register()
    {
        //
    }
}